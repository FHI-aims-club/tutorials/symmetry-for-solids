# The `rlsy_symmetry` implementation

Here, we will discuss the Si-diamond structure. The `geometry.in` for this structure should look like this:
```
lattice_vector 0.00000 2.71500 2.71500
lattice_vector 2.71500 0.00000 2.71500
lattice_vector 2.71500 2.71500 0.00000

atom_frac 0.00000 0.00000 0.00000 Si
atom_frac 0.25000 0.25000 0.25000 Si
```

## Detecting the space-group symmetry

In a fist step, you have to make sure that you set up your input structure with the proper space group symmetry. The symmetry in FHI-aims is determined from the input structure defined in the `geometry.in` file. Therefore, you should make sure your input structure has the desired symmetry. You can use the [*Structure Builder* of GIMS](https://gims.ms1p.org/static/index.html#StructureBuilder): click on *Structure Info* in the right-side panel to get the relevant symmetry information. Alternatively, you can use `clims` (`pip install --user clims`). By entering the command in the folder containing the above `geometry.in` file:
```
clims-unit-cell-info
```
you will receive the following information:

```
Structure Info
--------------
Number of atoms               : 2
Chemical formula              : Si2
Bravais Lattice               : face-centred cubic FCC(a=5.43)
Unit cell parameters          : 3.8395898218 3.8395898218 3.8395898218 60.0 60.0 60.0
Spacegroup number             : 227
Hall symbol                   : F 4d 2 3 -1d
Occupied Wyckoff positions    : b (1, 2)
Is primitive cell?            : True
Symmetry Threshold            : 1e-05
``` 

Indeed, we have setup the correct input structure for Si diamond, which has the spacegroup 227.


## Symmetry-constrained relaxation

!!! info 
    In FHI-aims, there are different ways to incorporate symmetry in your relaxation. Here, we focus on the one activated by the keyword `rlsy_symmetry`. Please also check-out the parametrically-constrained relaxation approach.

As a next step, we want to optimize the Si diamond structure. We will optimize all degrees of freedom, that is, lattice vectors and atom positions. 

One can constrain a system to keep its initial symmetry by setting the keyword `rlsy_symmetry` in the `control.in` file:
  
```
xc              pbe
relativistic    atomic_zora scalar
k_grid          13 13 13
relax_geometry  bfgs 5e-3
relax_unit_cell full
rlsy_symmetry   all

[attached light species defaults for Si]
```
    
The algorithms behind `rlsy_symmetry` reduce the number of real-space grid points and k-grid points by using the symmetry operations which are found from the given structure in `geometry.in`.

Start the calculation, so we can look at the final geometry with enforced symmetry. As a result you will find the following `geometry.in.next_step` file for the final structure:
```
#
# This is the geometry file that corresponds to the current relaxation step.
# If you do not want this file to be written, set the "write_restart_geometry" flag to .false.
#  aims_uuid : 51BDECA1-1475-4414-A89D-6268BAB7AE73
#
lattice_vector     -0.00000000      2.73828448      2.73828448
lattice_vector      2.73828448     -0.00000000      2.73828448
lattice_vector      2.73828448      2.73828448     -0.00000000
atom_frac      -0.00000000     -0.00000000      0.00000000 Si
atom_frac       0.25000000      0.25000000      0.25000000 Si
#
# What follows is the current estimated Hessian matrix constructed by the BFGS algorithm.
# This is NOT the true Hessian matrix of the system.
# If you do not want this information here, switch it off using the "hessian_to_restart_geometry" keyword.
#
trust_radius             0.2000000030
hessian_file
```

We can inspect the symmetry of the final structure again by using either GIMS or `clims`. For `clims`, you can type:
```
clims-unit-cell-info --filein geometry.in.next_step --format aims
```
and you will receive the following information:
```

Structure Info
--------------
Number of atoms               : 2
Chemical formula              : Si2
Bravais Lattice               : face-centred cubic FCC(a=5.47657)
Unit cell parameters          : 3.8725190493 3.8725190493 3.8725190493 60.0 60.0 60.0
Spacegroup number             : 227
Hall symbol                   : F 4d 2 3 -1d
Occupied Wyckoff positions    : b (1, 2)
Is primitive cell?            : True
Symmetry Threshold            : 1e-05

```

As desired, the final converged structure retains the perfect symmetry of the diamond structure, but with a slightly increased lattice constant. 

Let us compare the runtime for the unconstrained structure optimization. For this, please run a second calculation (in a separate folder) with the same input files. Only comment out the line `rlsy_symmetry all`. 

The overall computation time has been significantly reduced. It takes only 42s for the symmetry-reduced calculation on a single CPU, while it takes 188 s for the unconstrained relaxation. This is the result of reducing the number of real-space grid points by symmetry. The grid points have been roughly reduced by the number of symmetry operations. Thus, grid-based operations, such as integration or updates, take less time.

## Limitations of the rlsy_symmetry approach

Note that the rlsy_symmetry framework is only integrated for LDA, GGA, and meta-GGA functionals. For all functionals containing exact exchange, the rlsy_symmetry implementation has a negligible effect, since the real-space evaluation of the Fock matrix doesn't support symmetry reduction, yet. 