# Parametrically-constrained Relaxations

FHI-aims also accepts parametric constraints which, as an alternative to `rlsy_symmetry`, offer another way for performing symmetry constrained relaxation. The corresponding workflow (more complex but also more powerful; powerful in terms of that it is a more flexible framework for any constrain) is as follows:

1. Identify the Bravais lattice and figure out its prototype. Transform the lattice vectors into symbolic expressions using the degrees of freedom as parameters.
2. Identify the occupied Wyckoff positions. Transform the fractional coordinates of the atoms in your system and the lattice vectors into symbolic expressions.

The downside of the parametrically-constrained relaxations that it "only" constrains the degrees of freedom, but does not reduce the computational costs. 

As an example, using the Si-diamond structure, the `geometry.in` for a parametrically-constrained relaxations should look like this:

```
lattice_vector 0.00000 2.71500 2.71500
lattice_vector 2.71500 0.00000 2.71500
lattice_vector 2.71500 2.71500 0.00000

atom_frac 0.00000 0.00000 0.00000 Si
atom_frac 0.25000 0.25000 0.25000 Si
#=======================================================
# Parametric constraints
#=======================================================
symmetry_n_params 1 1 0
symmetry_params a
symmetry_lv 0, 0.5*a, 0.5*a
symmetry_lv 0.5*a, 0, 0.5*a
symmetry_lv 0.5*a, 0.5*a, 0
symmetry_frac 0, 0, 0
symmetry_frac 0.25, 0.25, 0.25
```

The first part is the standard definition of the input structure. The second part (everything after `# Parametric constraints`), defines the structure in symbolic expressions. The keyword have the following meaning:

* `symmetry_n_params`: The number of total, lattice, and atomic parameters, respectively.
* `symmetry_params`: The symbols for the parameters
* `symmetry_lv`: the lattice vectors in symbolic expressions
* `symmetry_frac`: the symbolic expression for the fractional atomic coordinates.

Note that the order of lattice vectors and atomic positions in the symbolic expression part must follow the same order as in the standard definition part further above. 

For the `control.in` file, you can set it up as for a standard relaxation:

```
xc                                 pbe
relativistic                       atomic_zora scalar
k_grid                             13 13 13
relax_geometry                     trm 5e-3
relax_unit_cell                    full

[attached light species defaults for Si]
```

Now, you can start the calculation. We can inspect the final geometry of the relaxation in the `geometry.in.next_step` file:

```
#
# This is the geometry file that corresponds to the current relaxation step.
# If you do not want this file to be written, set the "write_restart_geometry" flag to .false.
#  aims_uuid : BA90DDE4-B379-40B1-A680-4430BD376846
#
lattice_vector      0.00000000      2.73828444      2.73828444
lattice_vector      2.73828444      0.00000000      2.73828444
lattice_vector      2.73828444      2.73828444      0.00000000
atom_frac       0.00000000      0.00000000      0.00000000 Si
atom_frac       0.25000000      0.25000000      0.25000000 Si
#
# What follows is the current estimated Hessian matrix constructed by the BFGS algorithm.
# This is NOT the true Hessian matrix of the system.
# If you do not want this information here, switch it off using the "hessian_to_restart_geometry" keyword.
#
trust_radius             0.2000000030
hessian_block_lv             1       1           7.177404           0.000000           0.000000           0.000000           0.000000           0.000000           0.000000           0.000000           0.000000
```

Compare it to the result from the `rlsy_symmetry` approach. It is identical up to the numerically precision. If you want to restart the relaxation, do not forget to attach the *Parametric constraints* section.

To inspect the symmetry of the final geometry, you can use clims by typing:
```
clims-unit-cell-info --filein geometry.in.next_step --format aims
```
which will give the following output:
```

Structure Info
--------------
Number of atoms               : 2
Chemical formula              : Si2
Bravais Lattice               : face-centred cubic FCC(a=5.47657)
Unit cell parameters          : 3.8725189927 3.8725189927 3.8725189927 60.0 60.0 60.0
Spacegroup number             : 227
Hall symbol                   : F 4d 2 3 -1d
Occupied Wyckoff positions    : b (1, 2)
Is primitive cell?            : True
Symmetry Threshold            : 1e-05

```