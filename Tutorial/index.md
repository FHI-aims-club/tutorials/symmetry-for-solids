# Space-group symmetries for the simulation of solids

FHI-aims does not assume any real-space symmetry by default. However, for some purposes (e.g. the study of electronic and structural properties of high-symmetry phases) constraining the symmetry can be helpful - often, speed-up simulations can be achieved. In this tutorial you will how to constrain the real-space symmetry for a calculation. 

![](fig/cover-pic.png)

## Outline 

We will discuss the `rlsy_symmetry` implementation which uses real-space and k-space symmetries to reduce the dimensionality of the grids. This speeds up single point calculations and structure relaxations. You will learn about:

1. How to detect the symmetry of the system.
2. How to setup symmetry-constrained relaxations
3. Current limitations of the `rlsy_symmetry` implementation

In the second part we discuss, how the parametrically-constrained relaxation framework can be facilitated to allow for a symmetry constrained relaxation.


## Prerequisites

You should have:

* a basic understanding on how to setup a FHI-aims calculation,
* an installed FHI-aims executable with a version stamp `210716_3` or later,
* access to a computer with at least two (physical) cores.